import httpClient from "../utils/httpClient";

export const generateMachineStateAndTest = (data)=> {
    return httpClient.post('generate/states', data);
}