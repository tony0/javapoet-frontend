import axios from "axios";
import {baseUrlApi} from "./environment";

const httpClient = axios.create({
    baseURL: baseUrlApi,
    headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    }
});

export default httpClient;