import { Field, Form } from 'react-final-form';
import {Button, Col, Form as RForm} from "react-bootstrap";
import {TextFieldAdapter} from "./forms/FormAdapters";
import {useState} from "react";
import FormAddComponent from "./forms/FormAddComponent";
import FormRelationComponent from "./forms/FormRelationComponent";
import RenderItemComponent from "./items/RenderItemComponent";
import RenderItemRelationComponent from "./items/RenderItemRelationComponent";
import {generateMachineStateAndTest} from "../services/generatorService";

export function FormMachineStateComponent () {

    const [actions, setActions] = useState([]);
    const [states, setStates] = useState([]);
    const [initialState, setInitialState] = useState(undefined);
    const [relations, setRelations] = useState([]);

    const submitForm = (values) => {
        let objectName = values.objectName;
        const dto = {
            objectName: objectName,
            states: states,
            actions: actions,
            relations: relations
        }
        console.log(dto);
        generateMachineStateAndTest(dto).then(resp => {
            console.log(resp);
        }).catch(error => {
            console.log(error);
        });
        //TODO añadir servicio
    }

    const handleTypeForAction = (action) => {
        let list = actions.map(i => i);
        list.push(action);
        setActions(list);
        console.log(actions);
    }

    const handleTypeForState = (state) => {
        let list = states.map(s => s);
        list.push(state);
        setStates(list);
        if (!initialState) {
            setInitialState(states[0]);
        }
        console.log(states);
    }

    const handleAddedRelation = (relation) => {
        let list = relations.map(r => r);
        list.push(relation);
        setRelations(list);
        console.log(relations);
    }

    const handleInitialSelected = (selected) => {
        if (selected && selected === initialState) {
            console.log("son iguales");
        } else {
            setInitialState(selected);
            let list = [];
            list.push(selected);
            states.map(s => {
                if (s !== selected) {
                    list.push(s);
                }
            });
            setStates(list);
            console.log(states);
        }
    }

    return (
        <div>
            <FormAddComponent id="addActionForm" typeToAdd={'action'} addValueForType={handleTypeForAction}/>
            {actions.map(action => <RenderItemComponent id={"idItemComponentAction"+action} name={action}/>)}

            <FormAddComponent id="addStateForm" typeToAdd={'state'} addValueForType={handleTypeForState}/>
            <p className="text-info">Puede seleccionar el estado inicial del objeto, se mostrará al comienzo de los estados creados</p>
            {states.map(state => <RenderItemComponent id={"idItemComponentState"+state} name={state} selectInitial={handleInitialSelected}/>)}

            {
                states.length > 1 && actions.length > 0 &&
                <FormRelationComponent id={"addRelationForm"} states={states} actions={actions} addRelation={handleAddedRelation}/>
            }
            {
                states.length > 1 && actions.length > 0 &&
                relations.map(relation => <RenderItemRelationComponent relation={relation} id={"idRelationComponent"+relation.action+relation.actualState+relation.newState}/>)
            }

            <Form onSubmit={submitForm}
                render={({ handleSubmit, form, submitting, pristine, values}) => (
                    <form onSubmit={handleSubmit}>
                        <Col md={3}>
                            <RForm.Group controlId='objectName'>
                                <RForm.Label>Nombre del Objeto</RForm.Label>
                                <Field type='text' name='objectName' component={TextFieldAdapter}/>
                            </RForm.Group>
                        </Col>
                        <RForm.Group className='mt-3'>
                            <Col className='m-1'>
                                <Button id="submitGenerate" type='submit' variant='primary' disabled={submitting || pristine}>
                                    Generar Clases y Tests Java
                                </Button>
                            </Col>
                        </RForm.Group>
                    </form>
                )}
            />
        </div>

    );
}

export default FormMachineStateComponent;