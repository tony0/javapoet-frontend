import { Card, Col, Container, Row } from "react-bootstrap";
import FormMachineStateComponent from "./FormMachineStateComponent";


export function HomeComponent() {

    return (
        <Container className="p-1">
            Formulario de creación de máquina de estado
            <FormMachineStateComponent></FormMachineStateComponent>
        </Container>
    );
}

export default HomeComponent;