import { Route, Routes, useParams } from 'react-router-dom';
import HomeComponent from "./HomeComponent";

export function Main () {


    return (
        <div>
            <Routes>
                <Route path="home"
                       element={
                            <HomeComponent/>
                       }
                />
                <Route path='*' element={<HomeComponent/>} />
            </Routes>
        </div>
    );
}

export default Main;