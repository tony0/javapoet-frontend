import {Col, Row} from "react-bootstrap";

export const RenderItemRelationComponent = (props) => {
    const relation = props.relation;

    return (
        <Row className="text-center" md={6}>
            <Col>
                <div id={"relationIdActual"+relation.actualState} className='m-1 btn btn-outline-secondary outline-secondary disabled'>
                    {relation.actualState}
                </div>
            </Col>
            <Col className="m-1" md={1}>---------></Col>
            <Col>
                    <div id={"relationIdAction"+relation.action} className='m-1' >
                        {relation.action}
                    </div>
            </Col>
            <Col className="m-1" md={1}>---------></Col>
            <Col>
                <div id={"relationIdNew"+relation.newState} className='m-1 btn btn-outline-secondary outline-secondary disabled'>
                    {relation.newState}
                </div>
            </Col>
        </Row>
    );
}

export default RenderItemRelationComponent;