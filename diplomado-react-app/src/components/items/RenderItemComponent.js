
export const RenderItemComponent = (props) => {

    const name = props.name;

    const selectThis = () => {
        if (props.selectInitial) {
            props.selectInitial(name);
        }
    }

    if (props.selectInitial) {
        return (
            <div id={"itemId"+name} className='m-1 btn btn-outline-secondary outline-secondary' onClickCapture={selectThis}>
                {name}
            </div>
        );
    } else {
        return (
            <div id={"itemId"+name} className='m-1 btn btn-outline-secondary outline-secondary disabled'>
                {name}
            </div>
        );
    }
}

export default RenderItemComponent;