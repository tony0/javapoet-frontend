import {Button, Col, Form as RForm, Modal} from "react-bootstrap";
import {useState} from "react";
import { Field, Form } from 'react-final-form';
import {TextFieldAdapter} from "./FormAdapters";
import {EPart} from "../../utils/EPart";

const FormAddComponent = (props) => {
    const [show, setShow] = useState(false);
    const type = props.typeToAdd;
    const name = (type === EPart.ACTION) ? 'Acción' : 'Estado';

    const toggleModal = () => {
        setShow(!show);
    };

    const submitForm = (values) => {
        toggleModal();
        props.addValueForType(values.name);
    }

    return (
        <div className='my-2 mx-1'>
            <Button type='button' className='btn btn-success' onClick={toggleModal} id={name+"Modal"}>
                Añadir {name}
            </Button>
            <Modal show={show} onHide={toggleModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Añadir {name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={submitForm}
                      render={({ handleSubmit, form, submitting, pristine}) => (
                            <form onSubmit={handleSubmit}>
                                <Col>
                                    <RForm.Group controlId='name' className="p-2">
                                        <RForm.Label>Nombre:</RForm.Label>
                                        <Field type='text' name='name' placeholder='Nombre...' component={TextFieldAdapter}/>
                                    </RForm.Group>
                                </Col>
                                <RForm.Group className='mt-3'>
                                    <Col className='m-1'>
                                        <Button id="submitAdd" type='submit' variant='primary' disabled={submitting || pristine}>
                                            Añadir {name}
                                        </Button>
                                    </Col>
                                </RForm.Group>
                            </form>
                        )}
                    />
                </Modal.Body>
            </Modal>
        </div>
    );
};

export default FormAddComponent;