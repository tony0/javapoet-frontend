import {useState} from "react";
import {Button, Col, Form as RForm, Modal} from "react-bootstrap";
import { Field, Form } from 'react-final-form';
import {SelectFieldAdapter} from "./FormAdapters";

const FormRelationComponent = (props) => {
    const [show, setShow] = useState(false);
    const states = props.states;
    const actions = props.actions;

    const toggleModal = () => {
        setShow(!show);
    };

    const submitForm = (values) => {
        toggleModal();
        props.addRelation(values);
    }

    return (
        <div className='my-2 mx-1'>
            <Button type='button' className='btn btn-success' onClick={toggleModal} id='buttonRelationModal'>
                Añadir Relación
            </Button>
            <Modal show={show} onHide={toggleModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Añadir Relación</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={submitForm}
                          initialValues={{
                              actualState: states[0],
                              newState: states[1],
                              action: actions[0]
                          }}
                          render={({ handleSubmit, form, submitting, pristine, values}) => (
                              <form onSubmit={handleSubmit}>
                                  <Col>
                                      <RForm.Group controlId='actualState' className="p-2">
                                          <RForm.Label>Estado actual:</RForm.Label>
                                          <Field type='select' name='actualState' component={SelectFieldAdapter}>
                                              {states.map(s => <option id={s} value={s}>{s}</option>)}
                                          </Field>
                                      </RForm.Group>
                                  </Col>
                                  <Col>
                                      <RForm.Group controlId='newState' className="p-2">
                                          <RForm.Label>Nuevo estado:</RForm.Label>
                                          <Field type='select' name='newState' component={SelectFieldAdapter}>
                                              {states.map(s => <option id={s} value={s}>{s}</option>)}
                                          </Field>
                                      </RForm.Group>
                                  </Col>
                                  <Col>
                                      <RForm.Group controlId='action' className="p-2">
                                          <RForm.Label>Acción:</RForm.Label>
                                          <Field type='select' name='action' component={SelectFieldAdapter}>
                                              {actions.map(a => <option id={a} value={a}>{a}</option>)}
                                          </Field>
                                      </RForm.Group>
                                  </Col>

                                  <RForm.Group className='mt-3'>
                                      <Col className='m-1'>
                                          <Button id="submitAddRelation" type='submit' variant='primary'>
                                              Añadir Relación
                                          </Button>
                                      </Col>
                                  </RForm.Group>
                              </form>
                          )}
                    />
                </Modal.Body>
            </Modal>
        </div>
    );
};

export default FormRelationComponent;