import { Form } from "react-bootstrap";
import React from "react";

export const TextFieldAdapter = ({ input, meta, ...rest }) => (
    <>
        <Form.Control
            {...input}
            {...rest}
            isValid={!meta.error}
            isInvalid={meta.touched && meta.error}
        />
        <Form.Control.Feedback type="invalid">{meta.error}</Form.Control.Feedback>
    </>
);

export const SelectFieldAdapter = ({ input, ...rest }) => (
    <>
        <Form.Select
            {...input}
            {...rest}
        />
    </>
);

export const CheckboxAdapter = ({ input, meta, ...rest }) => (
    <>
        <Form.Check
            {...input}
            {...rest}
        />
    </>
);
