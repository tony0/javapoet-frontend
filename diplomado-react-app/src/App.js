import './App.css';
import {Component} from "react";
import {BrowserRouter} from "react-router-dom";
import Main from "./components/MainComponent";

class App extends Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {
//    <Provider store={store}>
//    </Provider>
    return (
          <BrowserRouter>
            <div>
                <Main/>
            </div>
          </BrowserRouter>

    );
  }
}

export default App;